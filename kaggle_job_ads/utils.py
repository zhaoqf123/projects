#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

import re
from pprint import pprint

from selenium import webdriver
import requests
from bs4 import BeautifulSoup
import pandas as pd


def parse_each_post_detail(each_post, attrs):
    all_names = list(each_post.find(attrs=attrs).stripped_strings)
    if len(all_names) != 1:
        raise ValueError("{} Names are not unique, please check!\n{}".format(attrs.values()[0], all_names))
    else:
        return all_names[0]

def parse_location_company(com_loc):
    """e.g. u'WINAMAX · Paris, France' """
    com, loc = com_loc.split(u"\xb7")
    return com.strip(), loc.strip()

def clean_view_count(view_count):
    view_count = view_count.strip()
    view_count = re.sub(r",", r"", view_count)
    return int(view_count)

def parse_each_post(each_post):
    href = u"https://www.kaggle.com" + each_post.get("href")
    position = parse_each_post_detail(each_post, attrs={"class": "position"})
    location = parse_each_post_detail(each_post, attrs={"class": "location"})
    com, loc = parse_location_company(location)
    post_date = parse_each_post_detail(each_post, attrs={"class": "post-date"})
    view_count = parse_each_post_detail(each_post, attrs={"class": "count"})
    view_count = clean_view_count(view_count)
    dict_detail = {u"href": href, u"position": position, u"company": com, u"location": loc, u"post_date": post_date, u"view_count": view_count}
    return dict_detail

def parse_all_posts(list_posts):
    list_post_detail = []
    for each_post in list_posts:
        parse_result = parse_each_post(each_post)
        list_post_detail.append(parse_result)
    return pd.DataFrame(list_post_detail)


class KaggleJobBoard(object):
    def __init__(self, url="https://www.kaggle.com/jobs"):
        self._url = url
        self._resp = None
        self._soup = None

    def _get_page(self):
        self._resp = requests.get(self._url)
    
    def _create_soup(self):
        if self._resp is None:
            self._get_page()
        self._soup = BeautifulSoup(self._resp.content, 'lxml')
    
    def parse_soup(self):
        if self._soup is None:
            self._create_soup()
        feature_posts = self._soup.find(attrs={"class": "section-header"}, string=re.compile("Featured")).parent
        recent_posts = self._soup.find(attrs={"class": "section-header"}, string=re.compile("Recent")).parent
        ## OR recent_posts = list(feature_posts.next_siblings)[1]
        list_feat_posts = feature_posts.find_all(attrs={"class": "job-post-row"})
        list_recent_posts = recent_posts.find_all(attrs={"class": "job-post-row"})
        df_feat_posts = parse_all_posts(list_feat_posts)
        df_feat_posts["property"] = ["featured"]*len(df_feat_posts)
        df_recent_posts = parse_all_posts(list_recent_posts)
        df_recent_posts["property"] = ["recent"]*len(df_recent_posts)
        df_merged = pd.concat([df_feat_posts, df_recent_posts])
        return df_merged.reset_index(drop=True)



























