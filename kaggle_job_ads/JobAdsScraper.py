# coding: utf-8
from __future__ import print_function
from datetime import datetime
import utils
import os

def scrape_main_page_every_day():
    file_name = datetime.strftime(datetime.now(), "%Y-%m-%d-%H-%M_SG.csv")
    file_path = "./data/output/main_page"
    file_name = os.path.join(file_path, file_name)
    kjb = utils.KaggleJobBoard("https://www.kaggle.com/jobs")
    df_result = kjb.parse_soup()
    df_result.to_csv(file_name, encoding="utf8", index=False)
    print("Successfully saved the file into location {}.".format(file_name))


if __name__ == "__main__":
    scrape_main_page_every_day()

















